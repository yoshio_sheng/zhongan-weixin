/**
 * Module dependencies.
 */
var cluster = require('cluster')
	, env = process.env.NODE_ENV || 'development';

var appName = "zhong'an-weixin";
var wx_token = process.env.WX_TOKEN || 'highteam_zhongan';

if (cluster.isMaster && env == 'production') {
	process.title = appName + ' master';
	console.log(process.title, 'started');
	    
	// Count the machine's CPUs
    var cpuCount = require('os').cpus().length;

    // Create a worker for each CPU
    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }
    
    process.on('SIGHUP', function() {
    	
    });

    cluster.on('death', function(worker) {
        console.log(appName, 'worker', '#' + worker.pid, 'died');
        cluster.fork();
    });
}
else {
	var express = require('express');
	var webot = require('weixin-robot');
	var http = require('http');
	var  env = process.env.NODE_ENV || 'development'
	  ,	config = require('./config/config')[env];
	var fs = require('fs');	
	var models_path = __dirname + '/app/models';
	fs.readdirSync(models_path).forEach(function (file) {
		if(file.toLowerCase().indexOf("relationship") == -1) {
			require(models_path+'/'+file)
		}
	})
	require(models_path+'/relationship');
	
	var app = express();
	// express settings
	require('./config/express')(app, config);
	
	// Bootstrap routes
	require('./app/web/routes')(app);
	
	// Start the app by listening on <port>
	var port = config.port || 5000
//	app.listen(port);
	
	http.createServer(app).listen(port, function(){
	    console.log('Express server listening on port ' + port);
	});
	
	webot.watch(app, { token: wx_token, path: '/wechat' });
	// webot.watch(app, { token: wx_token });
	require('./app/wechat/rules')(webot);
	
	process.title = appName + ' worker ';
    console.log(process.title, '#' + process.pid, 'started');
    
    process.on('SIGHUP', function() {
        process.exit(0);
    });
}