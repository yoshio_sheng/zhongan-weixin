var jugglingdb = require('jugglingdb')
  , Schema = jugglingdb.Schema
  , env = process.env.NODE_ENV || 'development'
  , config = require('../../config/config')[env];

var schema = new Schema('mysql', config.mysql);

var Bonus = schema.define('Bonus', {
	id: Number, 
	code: String,
	name: String,
	mobile: String,
	rewardtime: { type: Date, dataType: 'datetime' },
	status: Number,
	taobao:String,
	idcard:String,
	weixin:String
},
{
	table: 'za_bonus'
}
);

module.exports = schema.models.Bonus;