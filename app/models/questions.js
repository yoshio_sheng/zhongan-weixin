var jugglingdb = require('jugglingdb')
  , Schema = jugglingdb.Schema
  , env = process.env.NODE_ENV || 'development'
  , config = require('../../config/config')[env];

var schema = new Schema('mysql', config.mysql);

var Questions = schema.define('Questions', {
	id: Number, 
	content: String,
	name: String,
	mobile: String,
	userid: Number,
	createtime: { type: Date, dataType: 'datetime' }
},
{
	table: 'za_questions'
}
);

module.exports = schema.models.Questions;
