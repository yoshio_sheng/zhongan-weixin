var jugglingdb = require('jugglingdb')
  , Schema = jugglingdb.Schema
  , env = process.env.NODE_ENV || 'development'
  , config = require('../../config/config')[env]
  , Users = require('./users')
  , Questions = require('./questions')
  , Weixinlogs = require('./weixinlogs')
  , Bonus = require('./bonus')
;

module.exports = function(){
	
};

Bonus.execNativeQuery=Weixinlogs.execNativeQuery = Users.execNativeQuery = Questions.execNativeQuery 
= function(sql, cb) {
	Users.schema.adapter.query(sql, cb);
};