var jugglingdb = require('jugglingdb')
  , Schema = jugglingdb.Schema
  , env = process.env.NODE_ENV || 'development'
  , config = require('../../config/config')[env];

var schema = new Schema('mysql', config.mysql);

var Users = schema.define('Users', {
	id: Number, 
	name: String, 
	school: String, 
	schoollevel: String,
	schoolcard:String,
	mobile: String,
	idcard: String,
	createtime: { type: Date, dataType: 'datetime' },
	isactive: Number,
	weixinid: String
},
{
	table: 'za_users'
}
);

module.exports = schema.models.Users;
