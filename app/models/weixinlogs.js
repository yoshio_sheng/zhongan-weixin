var jugglingdb = require('jugglingdb')
  , Schema = jugglingdb.Schema
  , env = process.env.NODE_ENV || 'development'
  , config = require('../../config/config')[env];

var schema = new Schema('mysql', config.mysql);

var Weixinlogs = schema.define('Weixinlogs', {
	id: Number, 
	weixin_id: String,
	text: String,
	type: String,
	createtime: { type: Date, dataType: 'datetime' }
},
{
	table: 'za_weixinlogs'
}
);

module.exports = schema.models.Weixinlogs;
