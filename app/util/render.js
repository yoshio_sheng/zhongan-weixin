/**
 * util method
 */
var  env = process.env.NODE_ENV || 'development'
	, config = require('../../config/config')[env];

var ControllerRender = {};
ControllerRender.render = function(req, res, path, model){
	res.setHeader('Content-Type', 'text/html;charset=utf-8');
	if(model) {
		res.render(path, model);
	}
	else {
		res.render(path);
	}
};
ControllerRender.out = function(req, res, jsonObj){
	res.setHeader('Content-Type', 'application/json;charset=utf-8');
	res.json(jsonObj);
};
module.exports = ControllerRender;