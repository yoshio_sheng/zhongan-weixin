var Tools = require('../../util/tools')
	, viewHelper = require('../../util/render')
	, Users = require('../../models/users')
	, Questions = require('../../models/questions')
	, Bonus = require('../../models/bonus')
;
exports.register = function(req, res){
	var weixinid = req.param("weixin");
	var formUser = req.body;
	Users.create({
		name: formUser.name, 
		school: formUser.schoolname, 
		schoollevel: formUser.schoollevel,
		schoolcard:formUser.schoolcard,
		mobile: formUser.mobile,
		idcard: formUser.idcard,
		createtime: new Date(),
		isactive: 1,
		weixinid: weixinid
	}, function(err, user) {
		if(err){
			return res.status(500).json('500', { error: err.stack });
		}
		return viewHelper.out(req, res, user);
	});
};

exports.getUserInfoByWeixin = function(req, res){
	var weixinid = req.param("weixin");
	Users.findOne({where: {weixinid: weixinid}}, function(err, user){
		if(err){
			return res.status(500).json('500', { error: err.stack });
		}
		return viewHelper.out(req, res, user == null ? {weixinid:weixinid} : user );
	});
};

exports.question = function(req, res){
	var formQuestion = req.body;
	var ques = {
		name: formQuestion.name,
		mobile: formQuestion.mobile,
		content: formQuestion.question,
		createtime: new Date()
	};
	var userid = req.param("userid");
	if(Tools.hasText(userid) && userid != 'undefined'){
		ques.userid = userid;
	}
	Questions.create(ques, function(err, question) {
		if(err){
			return res.status(500).json('500', { error: err.stack });
		}
		return viewHelper.out(req, res, question);
	});
};

exports.bonus = function(req, res){
	var formBonus = req.body;
	var bonus = {
		rewardtime: new Date(),
		status: 1,
		taobao:formBonus.taobao,
		idcard:formBonus.idcard,
		weixin:req.param("weixin")
	};
	Bonus.find(req.param("bonusid"), function(err, bonusModel){
		if(err){
			return res.status(500).json('500', { error: err.stack });
		}
		if(bonusModel){
			if(bonusModel.status != 0){ // already updated
				return viewHelper.out(req, res, {created: false});
			}
			bonusModel.updateAttributes(bonus, function(err, updated){
				if(err){
					return res.status(500).json('500', { error: err.stack });
				}
				return viewHelper.out(req, res, {created: true});
			});
		}
		else{
			return res.status(500).json('500', { error: "Invalid Request" });
		}
	});
};