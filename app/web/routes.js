
module.exports = function (app) {
	var auth = require('../../config/middlewares/authorization');
	
	var appController = require('./controllers/appController');
	app.post("/za-server/register", auth.isAllowedIpAddress, appController.register);
	app.get("/za-server/getuserinfo", auth.isAllowedIpAddress, appController.getUserInfoByWeixin);
	app.post("/za-server/question", auth.isAllowedIpAddress, appController.question);
	app.post("/za-server/bonus", auth.isAllowedIpAddress, appController.bonus);
};