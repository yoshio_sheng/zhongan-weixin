var logCache = [];
var Weixinlogs = require('../models/weixinlogs');
var Bonus = require('../models/bonus');
var WechatRules = function(webot) {

	var APP_URL = "http://sanma.zhongan.com/za-app/index.html";
	var subscribeMsg = '感谢您关注“众安保险”官方微信公众帐号。众安保险是国内首家互联网保险公司，由阿里巴巴、腾讯、平安、携程等国内知名企业发起，基于”服务互联网”的宗旨，希望为所有互联网经济参与者提供保障和服务。您可以向我们随时提问，也可以定期获得众安保险最新活动信息参与互动，后续更多功能还在不断完善中，敬请期待。';
	
	var HP_INFO = {
		title : '首页',
//		url : APP_URL + "#home/",
		url : APP_URL + "#about/",
		picUrl : "http://zhongan.b0.upaiyun.com/1.0/weixin/pt_main.jpg",
		description : "点击进入众安保险官方微信首页"
	};
	
	var REG_INFO = {
		title : '我要报名',
		url : APP_URL + "#register/",
		picUrl : "http://zhongan.b0.upaiyun.com/1.1/weixin/pt_register.jpg",
		description : "点击进入报名页面"
	};
	
	var QUES_INFO = {
		title : '我要提问',
		url : APP_URL + "#ask/",
		picUrl : "http://zhongan.b0.upaiyun.com/1.1/weixin/pt_register.jpg",
		description : "点击进入提问页面"
	};
	
	var logWeixinInfo = function(info){
		info.logCreatedDate = new Date();
		logCache.push(info);
		process.nextTick(function(){
			var count = 0;
			var maxCount = 10;
			while(count < maxCount){
				var tmp = logCache.shift();
				if(tmp != null){
					Weixinlogs.create({
						weixin_id: tmp.uid,
						text: tmp.text == null ? tmp.param.eventKey : tmp.text,
						type: tmp.type,
						createtime: tmp.logCreatedDate
					}, function(err, question) { /*  */ });
					count++;
				}
				else{
					break;
				}
			}
		});
	};
	
	webot.set({
        name:'subscribe',
        description:'',
        pattern : function(info){
            return info.is('event') && info.param.event === 'subscribe';
        },
        handler : function(info){
        	logWeixinInfo(info);
            return subscribeMsg;
        }
    });
	
	webot.set({
		name : 'ignore',
		description : '众安保险-不回答文本消息',
		pattern: function(info) {
	    	return info.type == 'text';
	  	},
	  	handler: function(info) {
	  		info.noReply = true;
	  		return;
	  	}
	});
	/*
	webot.set({
		name : 'za_homepage',
		description : '众安保险-进入首页',
		pattern : function(info) {
			return info.is('event') && info.param.event === 'CLICK' && info.param.eventKey === 'homepage';
		},
		handler : function(info, next) {
			logWeixinInfo(info);
			HP_INFO.url = APP_URL + "#about/" + info.uid;
			return next(null, HP_INFO);
		}
	});
	
	webot.set({
		name : 'za_registerpage',
		description : '众安保险-我要报名',
		pattern : function(info) {
			return info.is('event') && info.param.event === 'CLICK' && info.param.eventKey === 'registerpage';
		},
		handler : function(info, next) {
			logWeixinInfo(info);
			REG_INFO.url = APP_URL + "#register/" + info.uid;
			return next(null, REG_INFO);
		}
	});
	*/
	webot.set({
		name : 'za_questionpage',
		description : '众安保险-我要提问',
		pattern : function(info) {
			return info.is('event') && info.param.event === 'CLICK' && info.param.eventKey === 'questionpage';
		},
		handler : function(info, next) {
			logWeixinInfo(info);
			QUES_INFO.url = APP_URL + "#ask/" + info.uid;
			return next(null, QUES_INFO);
		}
	});
	/*
	webot.set({
		name : 'za_bonuspage',
		description : '众安保险-有奖问答',
		pattern : function(info) {
			return info.type == 'text' && info.text == '有奖提问';
		},
		handler : function(info, next) {
			logWeixinInfo(info);
			info.session.nextTicket = 'rewardcode';
			return next(null, "请输入您的中奖编号");
		}
	});
	
	webot.set({
		name : 'za_bonuspage',
		description : '众安保险-有奖问答',
		pattern : function(info) {
			return info.type == 'text' && info.session.nextTicket == 'rewardcode';
		},
		handler : function(info, next) {
			logWeixinInfo(info);
			delete info.session.nextTicket;
			Bonus.findOne({where: {code: info.text}}, function(err, bonus){
				if(bonus){
					if(bonus.status == 1){
						return next(null, "您的信息正在核对中，我们的工作人员将尽快与您取得联系。");
					}
					if(bonus.status == 0){
						return next(null, "点击进入<a href='"+APP_URL + "#bonus/" + info.uid+"/"+bonus.id+"'>有奖问答</a>页面，完善您的个人信息。");
					}
				}
			});
		}
	});
    */
};

module.exports = WechatRules;