
var path = require('path')
  , rootPath = path.normalize(__dirname + '/..');

module.exports = {
  development: {
	  mysql: {
	      host: 'localhost',
	      port: 3306,
	      database: 'zhong-an',
	      username: 'root',
	      password: 'root', 
	      collation: "utf8_general_ci",
	      debug: true
	  },
	  root: rootPath,
	  allowedIpList: []
  },
  // the config for test env
  test: {},
  // the config for production env
  production: {
	  
  }
};