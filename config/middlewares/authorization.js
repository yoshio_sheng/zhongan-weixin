
/*
 *  Generic require login routing middleware
 */

var env = process.env.NODE_ENV || 'development'
 , config = require('../config')[env]
 , Tools = require(config.root + "/app/util/tools");
/**
 * check the request ip/domain whether allowed. 
 * if yes, process the api request,
 * otherwise, response 400 error. 
 */
exports.isAllowedIpAddress = function (req, res, next) {
	var requestIp = Tools.getRemoteAddress(req);
	if(requestIp.toLowerCase() == "localhost" || requestIp == "127.0.0.1" || requestIp == "0:0:0:0:0:0:1") {
		next();
		return;
	}
	for(var idx in config.allowedIpList) {
		if(requestIp == config.allowedIpList[idx]) {
			next();
			return;
		}
	}
    return res.status(400).send('400', { error: 'Access restricted' });
};