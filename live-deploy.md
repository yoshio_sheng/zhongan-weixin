#部署到live server方式：
		1.用项目根目录下的za.sql来创建我们需要用到的表结构。 

		2.修改config/config.js文件。 修改数据库连接的配置信息，debug设置为false

		3.修改app.js 端口改为默认80. 用node启动众安代码

		4.修改app/wechat/rules.js,将APP_URL地址修改为对应的live server域名

		5.将修改后的代码部署到live server,并且启动项目

		6.登录众安微信公众账号。转到高级-》开发者模式。
		
		7.在任意机器上安装webot-cli(npm install webot-cli -g). 使用webot-cli的 webot menu命令创建自定义菜单(菜单文件在项目下 weixin-menu.json以及weixin-menu1.json)
		
		8.打开众安微信公众账号开发者模式。换上对应服务器的地址http://xxxx.xxx.xx/wechat. token: highteam_zhongan
#自定义菜单的管理
##利用webot cli创建菜单
	webot menu create --appid xxxxx  --secret xxxxx <./weixin-menu2.json
	
	菜单的json文件格式可以在微信自定义菜单里面找的到

	appid 以及 secret请转到开发者模式，从上面获得
##webot cli删除菜单
	webot menu delete --appid xxxxx  --secret xxxxx

	PS:如果微信帐号之前有过自定义菜单，请先删除后再创建

##项目下面的几个菜单文件说明
	weixin-menu.json: 原始的菜单文件，包含首页，我要报名，我要提问。点击自定义菜单会先回复图文消息
	weixin-menu1.json: 只有关于众安的自定义菜单。点击直接进入关于众安页面
	weixin-menu2.json：关于众安以及我要提问菜单。点击关于众安直接进入页面，点击我要提问会先回复图文转接到提问页面。

#测试： 
取消关注众安微信公众账号，然后重新关注。可以看到最新的菜单以及代码部署情况。