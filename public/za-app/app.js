Ext.application({
    name: 'za',
    requires: ["Ext.data.Validations"],
    models: ['User', 'Question', 'Bonus'],
    views: ['HomePage', 'RegisterPage', 'NavigationBar', 'RegisterFormView', 'QuestionPage', 'QuestionFormView', 'MessagePage', 'BonusPage', 'BonusFormView', 'ArticleListView', 'BroadCastPage', 'ArticlePage', 'AboutPage','IntroPage'],
    controllers: ['Main'],
    
    launch: function() {
    	document.getElementById("loading").style.display = "none";
		// 注册身份证号码验证器
		Ext.applyIf(Ext.data.Validations, {
			idcard: function (config, value) {
				if(config == null){
					value = config;
				}
				else if(!config.required && value.length < 1){
					return true;
				}
				
				if(value.length != 15 && value.length != 18){
					return false;
				}
				var idCard15To18 = function(id){
				  var W = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2, 1);
				  var A = new Array("1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2");
				  var i,j,s=0;
				  var newid;
				  newid = id;
				  newid = newid.substring(0,6)+"19"+newid.substring(6,id.length);
				  for(i=0;i<newid.length;i++ ){
				    j= parseInt(newid.substring(i,i+1))*W[i];
				    s=s+j;
				  }
				  s = s % 11;
				  newid=newid+A[s]; 
				  return newid;
				};
				var isIdCard = function(arrIdCard){
					var tag = false;	
					var sigma = 0;  
					var a = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 );  
					var w = new Array("1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2");  		
					for (var i = 0; i < 17; i++) {  
						var ai = parseInt(arrIdCard.substring(i, i + 1));  
						var wi = a[i];  
						sigma += ai * wi;  			
					}   
					var number = sigma % 11;    		
					var check_number = w[number];   
					if (arrIdCard.substring(17) != check_number) {  
						tag =  false;  
					} else {  
						tag = true;  
					}  	
					return tag;
				};
				// 如果是15位身份证号,先转成18位的
				if(value.length == 15){
					value = idCard15To18(value);
				}
				return isIdCard(value);
			}
		});
    }
});
