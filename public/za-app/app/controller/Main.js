Ext.define('za.controller.Main', {
    extend: 'Ext.app.Controller',
    
    config: {
        viewCache: {},

        refs: {
        	homePageUI: 'homepage',
        	backHomeButton: "button[action=backToHomePage]",
        	backHistoryButton: "button[action=backToHistory]",
        	backAskButton : "button[action=backToAskPage]",
        	registerSubmitButton: '#registerSubmitButton',
        	questionSubmitButton: "#questionSubmitButton",
        	questionContinueButton: "#questionContinueButton",
        	bonusSubmitButton: "#bonusSubmitButton"
        },
        
        control: {
        	homePageUI: {
                itemtap: 'onMenuTap'
            },
            backHomeButton:{tap:'loadHomePage'},
            backHistoryButton:{tap:'backToHistory'},
            backAskButton:{tap:'loadAskPage'},
            registerSubmitButton: {tap:"submitRegisterForm"},
            questionSubmitButton:{tap: "submitQuestionForm"},
            questionContinueButton:{tap: "submitQuestionFormAndContinue"},
            bonusSubmitButton:{tap: "submitBonusForm"}
        },
        
        routes: {
        	  ":page/:weixin": "loadPageModule",
        	  "bonus/:weixin/:bonusID": "loadBonusPageModule",
        	  "article/:weixin/:articleID": "loadArticleDetailPageModule"
        },
        
        currentPage: undefined,
        viewHistory: []
    },
    
    init:function(){
    	Ext.PageCacheObject = {Ctl: this, LoggedInUser: null};
    },
    
    getWeixinID : function(){
    	return sessionStorage.getItem("WEIXIN_ID");
    },
    
    setWeixinID : function(weixin){
    	sessionStorage.setItem("WEIXIN_ID", weixin);
    },
    
    loadBonusPageModule:function(weixin, bonusID){
    	this.loadPageModule('bonus', weixin, function(){
    		Ext.PageCacheObject.LoggedInUser.bonusID = bonusID;
    	});
    },
    
    loadArticleDetailPageModule : function(weixin, articleID){
    	this.loadPageModule('article', weixin, function(){
    		Ext.PageCacheObject.LoggedInUser.articleID = articleID;
    	});
    },
    
    loadPageModule:function(page, weixin, callback){
    	var _this = this;
    	var redirectPage = function(page){
    		if(callback){
    			callback();
    		}
    		if(page == "register"){
    			_this.logViewHistory(_this._getHomePageCache());
    			_this.loadRegisterPage();
        	}
        	else if(page == "ask"){
        		_this.logViewHistory(_this._getHomePageCache());
        		_this.loadAskPage();
        	}
    		else if(page == "bonus"){
        		_this.logViewHistory(_this._getHomePageCache());
        		_this.loadBonusPage();
        	}
    		else if(page == "zhibo"){
        		_this.logViewHistory(_this._getHomePageCache());
        		_this.loadBroadcastPage();
        	}
    		else if(page == "article"){
    			_this.logViewHistory(_this._getHomePageCache());
//    			_this.logViewHistory(_this._getBroadcastPageCache());
        		_this.loadArticleDetailPage(Ext.PageCacheObject.LoggedInUser.articleID);
        		delete Ext.PageCacheObject.LoggedInUser.articleID;
    		}
    		else if(page == "about"){
    			_this.logViewHistory(_this._getHomePageCache());
    			_this.loadAboutPage();
    		}
            else if(page == "intro"){
                _this.logViewHistory(_this._getHomePageCache());
                _this.loadIntroPage();
            }
        	else{
        		_this.loadHomePage();
        	}
    	};
    	if(Ext.PageCacheObject.LoggedInUser != null){
    		redirectPage(page);
    		return;
    	}
    	else{
    		if(weixin == "WEIXIN_REPLACE"){
    			weixin = this.getWeixinID();
    		}
    		else{
    			this.setWeixinID(weixin);
    		}
    		Ext.Ajax.request({
        	    url: '/za-server/getuserinfo?weixin=' + weixin,
        	    method: 'GET',
        	    callback: function(options, success, response) {
        	        if (success) {
        	        	Ext.PageCacheObject.LoggedInUser = JSON.parse(response.responseText);
        	        	redirectPage(page);
        	        } else {
        	            Ext.Msg.alert('服务器错误', '貌似服务器出了问题，请您稍后再试！');
        	        }
        	    }
        	});
    	}
    },
    
    logViewHistory: function(pageView){
    	if(this.getViewHistory().length > 5){
    		this.getViewHistory().shift();
    	}
    	this.getViewHistory().push(pageView);
    },
    
    backToHistory:function(){
    	if(this.getViewHistory().length <= 1){
    		return;
    	}
    	var pageView = this.getViewHistory()[this.getViewHistory().length - 2];
    	if(pageView != null){
    		this.getViewHistory().pop();
    		Ext.Viewport.setActiveItem(pageView);
    	}
    },
    
    _getHomePageCache: function(){
    	var homePage = this.getViewCache()["za.homepage.cache"];
    	if(homePage == null){
    		homePage = Ext.create('za.view.HomePage');
    		this.getViewCache()["za.homepage.cache"] = homePage;
    	}
    	return homePage;
    },

    loadHomePage:function(){
    	var homePage = this._getHomePageCache();
    	Ext.Viewport.setActiveItem(homePage);
    	this.logViewHistory(homePage);
    },
    
    loadRegisterPage: function(){
    	if(Ext.PageCacheObject.LoggedInUser.id != null){
    		this.loadMessagePage('亲～您的报名信息已经提交成功！由于报名人数太多，小安会随机筛选出60人，在<span style="color:red;">11月1日</span>通过微信统一发送入场电子凭证，请耐心等候哦！<span style="color:red;">收到二维码凭证后请妥善保管，一旦遗失，后果自负。</span><br/><br/>PS:如果没有收到入场凭证，'+
    		'还可以场外通过微信或者众安微博与三马互动，获得创业支持神秘礼品。');
    		return;
    	}
    	var registerPage = this.getViewCache()["za.registerpage.cache"];
    	if(registerPage == null){
    		registerPage = Ext.create('za.view.RegisterPage');
    		this.getViewCache()["za.registerpage.cache"] = registerPage;
    	}
    	registerPage.innerItems[0].reset();
    	this._resetForm(registerPage.innerItems[0]);
    	Ext.Viewport.setActiveItem(registerPage);
    	this.logViewHistory(registerPage);
    },
    
    loadAskPage: function(){
    	var questionPage = this.getViewCache()["za.questionpage.cache"];
    	if(questionPage == null){
    		questionPage = Ext.create('za.view.QuestionPage');
    		this.getViewCache()["za.questionpage.cache"] = questionPage;
    	}
    	questionPage.innerItems[0].reset();
    	this._resetForm(questionPage.innerItems[0]);
    	if(Ext.PageCacheObject.LoggedInUser.id != null){
    		questionPage.innerItems[0].setValues({
				name: Ext.PageCacheObject.LoggedInUser.name,
				mobile: Ext.PageCacheObject.LoggedInUser.mobile
			}); 
    	}
    	Ext.Viewport.setActiveItem(questionPage);
    	this.logViewHistory(questionPage);
    },
    
    loadBonusPage: function(){
		var bonusPage = this.getViewCache()["za.bonuspage.cache"];
		if(bonusPage == null){
			bonusPage = Ext.create('za.view.BonusPage');
			this.getViewCache()["za.bonuspage.cache"] = bonusPage;
		}
		bonusPage.innerItems[0].reset();
		this._resetForm(bonusPage.innerItems[0]);
		Ext.Viewport.setActiveItem(bonusPage);
		this.logViewHistory(bonusPage);
    },
    
    loadAboutPage: function(){
		var aboutPage = this.getViewCache()["za.aboutpage.cache"];
		if(aboutPage == null){
			aboutPage = Ext.create('za.view.AboutPage');
			this.getViewCache()["za.aboutpage.cache"] = aboutPage;
		}
		Ext.Viewport.setActiveItem(aboutPage);
		this._adjustAboutHeiht("aboutPageContent");
		this.logViewHistory(aboutPage);
    },

    loadIntroPage: function(){
        var introPage = this.getViewCache()["za.intropage.cache"];
        if(introPage == null){
            introPage = Ext.create('za.view.IntroPage');
            this.getViewCache()["za.intropage.cache"] = introPage;
        }
        Ext.Viewport.setActiveItem(introPage);
        this._adjustAboutHeiht("intropageContent");
        this.logViewHistory(introPage);
    },
    
    showMoreAboutContent:function(alink){
    	document.getElementById("moreContent_1").style.display = "";
    	document.getElementById("moreContent_2").style.display = "";
    	document.getElementById("moreContent_3").style.display = "";
    	this._adjustAboutHeiht();
    	alink.style.display = "none";
    	var aboutPage = this.getViewCache()["za.aboutpage.cache"];
    	aboutPage.setScrollable(true);
    },
    
    _adjustAboutHeiht: function(id){
    	var offsetTop = 0.382 * document.body.clientHeight;
    	var aboutPageContent = document.getElementById(id);
		var aboutBgHeight = 1.575 * document.body.clientWidth;
		if((aboutPageContent.clientHeight + offsetTop) > aboutBgHeight){
			document.getElementById("tmpHeightContent").style.height = aboutPageContent.clientHeight + offsetTop + 20 - aboutBgHeight + "px";
		}
		aboutPageContent.style.top = offsetTop+ "px";
    },
    
    _getBroadcastPageCache:function(){
    	var broadcastPage = this.getViewCache()["za.broadcastpage.cache"];
		if(broadcastPage == null){
			broadcastPage = Ext.create('za.view.BroadCastPage');
			this.getViewCache()["za.broadcastpage.cache"] = broadcastPage;
		}
		return broadcastPage;
    },
    
    loadBroadcastPage:function(){
    	var broadcastPage = this._getBroadcastPageCache();
		Ext.Viewport.setActiveItem(broadcastPage);
		this.logViewHistory(broadcastPage);
    },
    
    loadArticleDetailPage:function(articleID){
    	var me = this;
    	var articlePage = this.getViewCache()["za.articlepage.cache"];
		if(articlePage == null){
			articlePage = Ext.create('za.view.ArticlePage');
			this.getViewCache()["za.articlepage.cache"] = articlePage;
		}
		Ext.Viewport.setActiveItem(articlePage);
		Ext.Viewport.setMasked({
            xtype:'loadmask',
            message:'正在加载...'
        });
		Ext.Ajax.request({
    	    url: '/za-app/testArticle'+articleID+'.json',
    	    method: 'GET',
    	    callback: function(options, success, response) {
    	        if (success) {
    	        	var articleJson = JSON.parse(response.responseText);
    	        	Ext.getCmp("articlePageToolbar").setTitle(truncateString(articleJson.title, 13));
    	        	document.getElementById("articlePageTitle").innerHTML = articleJson.title;
    	        	document.getElementById("articlePageDate").innerHTML = articleJson.date;
    	        	document.getElementById("articlePageContent").innerHTML = articleJson.content;
    	        	window.setTimeout(function(){
    	        		Ext.Viewport.setMasked(false);
    	        		var offsetTop = 10;
        	        	var aboutPageContent = document.getElementById("articlepagecontentdiv");
        	    		document.getElementById("tmpHeightContent").style.height = aboutPageContent.clientHeight + offsetTop + 20 + "px";
        	    		aboutPageContent.style.top = offsetTop+ "px";
    	        	}, 1500);
    	        	var offsetTop = 10;
    	        	var aboutPageContent = document.getElementById("articlepagecontentdiv");
    	    		document.getElementById("tmpHeightContent").style.height = aboutPageContent.clientHeight + offsetTop + 20 + "px";
    	    		aboutPageContent.style.top = offsetTop+ "px";
    	        	/*
    	        	window.setTimeout(function(){
    	        		document.getElementById("articlepagescreen").style.height = document.getElementById("articlePageTitle").clientHeight + 
        	        	document.getElementById("articlePageDate").clientHeight +document.getElementById("articlePageContent").clientHeight + 80 + "px";
    	        	}, 500);*/
    	        } else {
    	        	Ext.Viewport.setMasked(false);
    	            Ext.Msg.alert('服务器错误', '貌似服务器出了问题，请您稍后再试！');
    	        }
    	    }
    	});
		this.logViewHistory(articlePage);
    },
    
    _resetForm : function(form){
    	var fields = form.query("field");
		// remove the style class from all fields
		for (var i=0; i<fields.length; i++) {
		    fields[i].removeCls('invalidField');
		    if(form.getId() == "registerForm"){
		    	var append = fields[i].getName();
		    	if(fields[i].getName() == "name"){
    				append = "username";
    			}
    			else if(fields[i].getName() == "mobile"){
    				append = "usermobile";
    			}
		    	document.getElementById(append+"error").innerHTML = "";
		    }
		    else{
		    	document.getElementById(fields[i].getName()+"error").innerHTML = "";
		    }
		}
    },
    
    submitRegisterForm:function(){
    	var _this = this;
    	var registerForm = Ext.getCmp("registerForm");
    	this._resetForm(registerForm);
    	var user = Ext.create("za.model.User", registerForm.getValues());
    	var errors = user.validate();
    	if (!errors.isValid()) {
    		errors.each(function (errorObj){
    			var s = Ext.String.format('field[name={0}]',errorObj.getField());
    			var append = errorObj.getField();
    			if(errorObj.getField() == "name"){
    				append = "username";
    			}
    			else if(errorObj.getField() == "mobile"){
    				append = "usermobile";
    			}
    			document.getElementById(append+"error").innerHTML = errorObj.getMessage();
    			registerForm.down(s).addCls('invalidField');
    	    });
    	}
    	else{
    		Ext.Viewport.setMasked({
                xtype:'loadmask',
                message:'正在处理...'
            });
	    	Ext.Ajax.request({
	    	    url: '/za-server/register?weixin='+Ext.PageCacheObject.LoggedInUser.weixinid,
	    	    params: registerForm.getValues(),
	    	    method: 'POST',
	    	    callback: function(options, success, response) {
	    	    	Ext.Viewport.setMasked(false);
	    	        if (success) {
	    	        	Ext.PageCacheObject.LoggedInUser = JSON.parse(response.responseText);
	    	        	_this.loadMessagePage('亲～您的报名信息已经提交成功！由于报名人数太多，小安会随机筛选出60人，在<span style="color:red;">11月1日</span>通过微信统一发送入场电子凭证，请耐心等候哦！<span style="color:red;">收到二维码凭证后请妥善保管，一旦遗失，后果自负。</span><br/><br/>PS:如果没有收到入场凭证，'+
	    	        		'还可以场外通过微信或者众安微博与三马互动，获得创业支持神秘礼品。');
	    	        } else {
	    	            Ext.Msg.alert('服务器错误', '貌似服务器出了问题，请您稍后再试！');
	    	        }
	    	    }
	    	});
    	}
    },
    
    submitQuestionForm: function(){
    	this.doSubmitQuestion(false);
    },
    
    submitQuestionFormAndContinue: function(){
    	this.doSubmitQuestion(true);
    },
    
    hideKeyboard : function () {
	     var activeElement = document.activeElement;
	     activeElement.setAttribute('readonly', 'readonly'); 
	     activeElement.setAttribute('disabled', 'true');
	     Ext.defer(function() {
	         activeElement.blur();
	         activeElement.removeAttribute('readonly');
	         activeElement.removeAttribute('disabled');
	     }, 100);
	},
    
    doSubmitQuestion : function(isContinue){
		this.hideKeyboard();
    	isContinue = isContinue || false;
    	var _this = this;
    	var questionForm = Ext.getCmp("questionForm");
    	this._resetForm(questionForm);
    	document.getElementById("instTip").innerHTML = "";
    	var question = Ext.create("za.model.Question", questionForm.getValues());
    	var errors = question.validate();
    	if (!errors.isValid()) {
    		errors.each(function (errorObj){
    			var s = Ext.String.format('field[name={0}]',errorObj.getField());
    			document.getElementById(errorObj.getField()+"error").innerHTML = errorObj.getMessage();
    			questionForm.down(s).addCls('invalidField');
    	    });
    	}
    	else{
    		Ext.Viewport.setMasked({
                xtype:'loadmask',
                message:'正在处理...'
            });
	    	Ext.Ajax.request({
	    	    url: '/za-server/question?userid='+Ext.PageCacheObject.LoggedInUser.id,
	    	    params: questionForm.getValues(),
	    	    method: 'POST',
	    	    callback: function(options, success, response) {
	    	    	Ext.Viewport.setMasked(false);
	    	        if (success) {
	    	        	if(isContinue){
	    	        		var quesObj = JSON.parse(response.responseText);
	    	        		document.getElementById("instTip").innerHTML = "您的问题已经提交，请输入您的新问题！";
	    	        		questionForm.reset();
	    	        		questionForm.setValues({
	    	    				name: quesObj.name,
	    	    				mobile: quesObj.mobile
	    	    			}); 
	    	        	}
	    	        	else{
	    	        		_this.loadMessagePage('亲~我们已收到您的提问！提问内容或有机会于11月6日“互联网金融”论坛暨众安保险开幕仪式现场由“三马”作亲自解答！届时，众安保险新浪及腾讯微博、微信官方账号上会进行现场微直播。敬请期待哦！', true);
	    	        	}
	    	        } else {
	    	            Ext.Msg.alert('服务器错误', '貌似服务器出了问题，请您稍后再试！');
	    	        }
	    	    }
	    	});
    	}
    },
    
	submitBonusForm:function(){
		var _this = this;
		var bonusForm = Ext.getCmp("bonusForm");
		this._resetForm(bonusForm);
		var bonus = Ext.create("za.model.Bonus", bonusForm.getValues());
		var errors = bonus.validate();
		if (!errors.isValid()) {
			errors.each(function (errorObj){
				var s = Ext.String.format('field[name={0}]',errorObj.getField());
				var append = errorObj.getField();
				document.getElementById(append+"error").innerHTML = errorObj.getMessage();
				bonusForm.down(s).addCls('invalidField');
		    });
		}
		else{
			if(bonusForm.getValues().taobao.length < 1 && bonusForm.getValues().idcard.length < 1){
				document.getElementById("taobaoerror").innerHTML = "请至少输入一项，以便我们核对您的信息。";
				return;
			}
			Ext.Viewport.setMasked({
	            xtype:'loadmask',
	            message:'正在处理...'
	        });
	    	Ext.Ajax.request({
	    	    url: '/za-server/bonus?bonusid='+Ext.PageCacheObject.LoggedInUser.bonusID+'&weixin='+Ext.PageCacheObject.LoggedInUser.weixinid,
	    	    params: bonusForm.getValues(),
	    	    method: 'POST',
	    	    callback: function(options, success, response) {
	    	    	Ext.Viewport.setMasked(false);
	    	        if (success) {
	    	        	delete Ext.PageCacheObject.LoggedInUser.bonusID;
	    	        	var quesObj = JSON.parse(response.responseText);
	    	        	if(quesObj.created){
	    	        		_this.loadMessagePage('亲～您的信息已经提交成功！我们的工作人员将会尽快与您取得联系。在此感谢您的积极参与，恭喜您获奖！');
	    	        	}
	    	        	else{
	    	        		_this.loadMessagePage('亲～您的信息正在审核中！请勿重复提交，我们的工作人员将会尽快与您取得联系。');
	    	        	}
	    	        } else {
	    	            Ext.Msg.alert('服务器错误', '貌似服务器出了问题，请您稍后再试！');
	    	        }
	    	    }
	    	});
		}
	},
    /*
    loadMessagePage:function(messageHtml, showBackAskBtn){
    	showBackAskBtn = showBackAskBtn || false;
    	var messagePage = this.getViewCache()["za.messagepage.cache"];
    	if(messagePage == null){
    		messagePage = Ext.create('za.view.MessagePage');
    		this.getViewCache()["za.messagepage.cache"] = messagePage;
    	}
    	document.getElementById("messagebodyarea").innerHTML = messageHtml;
    	var btn = this.getBackAskButton();
    	btn.setHidden(true);
    	if(showBackAskBtn){
    		btn.setHidden(false);
    	}
    	Ext.getCmp("msgBackButton").setHidden(true);
    	Ext.Viewport.setActiveItem(messagePage);
    	this.logViewHistory(messagePage);
    }*/
	loadMessagePage: function(messageHtml, showBackAskBtn){
    	var messagePage = this.getViewCache()["za.messagepage.cache"];
    	if(messagePage == null){
    		messagePage = Ext.create('za.view.MessagePage');
    		this.getViewCache()["za.messagepage.cache"] = messagePage;
    	}
    	document.getElementById("messagebodyarea").innerHTML = messageHtml;
        Ext.Viewport.setActiveItem(messagePage);
        this._adjustAboutHeiht("messagepageContent");
        var left = (document.getElementById("messagebtnarea").clientWidth - 163) /2;
    	document.getElementById("backAndContinueAskButton").style.marginLeft = left + "px";
        this.logViewHistory(messagePage);
    }
});
