Ext.define('za.model.Bonus', {
    extend: 'Ext.data.Model',
    config: {
        fields: ['taobao', 'idcard'],
		validations : [
        {
		    type : 'idcard',
		    field : 'idcard',
		    required: false,
		    message : "请输入合法的身份证号码"
		}]
    }
});
