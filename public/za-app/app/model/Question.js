Ext.define('za.model.Question', {
    extend: 'Ext.data.Model',
    config: {
        fields: ['question', 'name', 'mobile'],
		validations : [{
		    type : 'presence',
		    field : 'name',
		    message : "请输入您的姓名"
		},
		{ type: 'length', field: 'name', max: 20, min:2, message: "您输入的姓名格式有误." },
		{
		    type : 'format',
		    field : 'name',
		    matcher : /^([\u4E00-\uFA29]|[\uE7C7-\uE7F3]|[a-zA-Z]|[\s])*$/,
		    message : "您输入的姓名格式有误."
		},
		{
		    type : 'presence',
		field : 'question',
		    message : "请输入您的问题"
		}, 
		{
		    type : 'presence',
		field : 'mobile',
		    message : "请输入手机号码"
		},
		{ type: 'length', field: 'mobile', max: 11, min:11, message: "请正确输入11位手机号码" },
        {
		    type : 'format',
		    field : 'mobile',
		    matcher : /^0?(13[0-9]|15[0123456789]|18[0123456789]|14[57])[0-9]{8}$/,
		    message : "您输入的手机号码不正确"
		}]
    }
});
