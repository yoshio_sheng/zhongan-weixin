Ext.define('za.model.User', {
    extend: 'Ext.data.Model',
    config: {
        fields: ['name', 'schoolname', 'schoollevel', 'schoolcard', 'mobile'/*, 'idcard'*/],
		validations : [{
		    type : 'presence',
		    field : 'name',
		    message : "请输入您的姓名"
		},
		{ type: 'length', field: 'name', max: 7, min:2, message: "您输入的姓名长度不合法！" },
		{
		    type : 'format',
		    field : 'name',
		    matcher : /^([\u4E00-\uFA29]|[\uE7C7-\uE7F3]|[a-zA-Z]|[\s])*$/,
		    message : "您的姓名含有非法字符，仅允许中文，英文，空格"
		},
		{
		    type : 'presence',
		field : 'schoolname',
		    message : "请输入学校名称"
		}, 
		{
		    type : 'format',
		    field : 'schoolname',
		    matcher : /^([\u4E00-\uFA29]|[\uE7C7-\uE7F3]|[a-zA-Z]|[\s])*$/,
		    message : "请正确输入您的学校名称，只能含中文，英文，空格"
		},
		{
		    type : 'presence',
		field : 'schoollevel',
		    message : "请输入您的专业"
		}, 
		{
		    type : 'format',
		    field : 'schoollevel',
		    matcher : /^([\u4E00-\uFA29]|[\uE7C7-\uE7F3]|[a-zA-Z]|[\s])*$/,
		    message : "请正确输入您的专业名称，只能含中文，英文，空格"
		},
		{
		    type : 'presence',
		field : 'schoolcard',
		    message : "请输入学生证号"
		}, 
		{
		    type : 'format',
		    field : 'schoolcard',
		    matcher : /^([a-zA-Z]|[\d])*$/,
		    message : "请正确输入您的专业名称，只能含字母或数字"
		},
		{
		    type : 'presence',
		field : 'mobile',
		    message : "请输入手机号码"
		}, 
		{ type: 'length', field: 'mobile', max: 11, min:11, message: "请正确输入11位手机号码" },
        {
		    type : 'format',
		    field : 'mobile',
		    matcher : /^0?(13[0-9]|15[0123456789]|18[0123456789]|14[57])[0-9]{8}$/,
		    message : "您输入的手机号码不正确"
		}
		/*,
		{
		    type : 'presence',
		field : 'idcard',
		    message : "请输入身份证号"
		}, 
        {
		    type : 'idcard',
		    field : 'idcard',
		    message : "请输入合法的身份证号码"
		}*/
		]
    }
});
