Ext.define('za.view.AboutPage', {
    extend: 'Ext.Panel',
    xtype: "aboutpage",
    requires: [
               'za.view.NavigationBar'
           ],
    config: {
        fullscreen: true,   
        scrollable: true,
        items: [
            {
                id: 'aboutpagescreen',
                xtype: 'panel', // <a '+ (Ext.os.is.Phone ? "ontouchstart" : "onclick") +'="Ext.PageCacheObject.Ctl.showMoreAboutContent(this);" style="color:#2980b9">查看更多...</a>
                style: "width:100%;-webkit-text-size-adjust: none;-webkit-user-select: none;padding:0px;margin: 0; background: #ffffff;background-size: cover;",
                html:  '<img id="aboutBgImg" src="http://zhongan.b0.upaiyun.com/1.5/msite/bg_about_us.jpg" style="width:100%;" /><div id="tmpHeightContent"></div><div id="aboutPageContent" style="position:absolute;font-size:17px;font-weight:2em;padding:5px;margin-bottom:20px;line-height:22px;color:#706e6f;top:150px;"><div id="moreContent_4" style="text-align: justify;">　　互联网已经在我国国民经济中占有重要地位，它改变了人们生活的方方面面。但互联网在发展中也遇到了一系列不同于传统行业的风险和瓶颈。众安保险是国内首家互联网保险公司，是由阿里、腾讯、携程等互联网领先企业，基于保障和促进整个互联网行业发展的初衷，与平安等金融企业共同打造。</div>'+
                        '<div id="moreContent_3" style="text-align: justify;">　　众安保险的定位是“服务互联网”。众安保险不只是通过互联网销售既有的保险产品，而是通过产品创新，为互联网的经营者和参与者提供一系列整体解决方案，化解和管理互联网经济的各种风险，为互联网行业的顺畅、安全、高效运行提供保障和服务。</div>'+
                        '</div>'
            }/*,
            {
                id: 'articlePageToolbar',
                xtype: 'pagenavigationbar',
                title: '关于众安',
                docked: 'top',
                items: [{
                    xtype : 'button',
                    id: 'viewSourceButton',
                    hidden: false,
                    align : 'right',
                    action: 'backToHomePage',
                    text  : '首页'
                },
                {
                    text: 'Back',
                    ui: 'back',
                    align : 'left',
                    action: 'backToHistory'
                }
                ]
            }*/
        ]
    }
});
