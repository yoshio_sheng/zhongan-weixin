/**
 * Demonstrates a tabbed form panel. This uses a tab panel with 3 tabs - Basic, Sliders and Toolbars - each of which is
 * defined below.
 *
 * See this in action at http://dev.sencha.com/deploy/sencha-touch-2-b3/examples/kitchensink/index.html#demo/forms
 */
Ext.define('za.view.ArticleListView', {
    extend: 'Ext.DataView',
    xtype: "articlelist",
    
    config: {
    	store: {
            fields: ['imgUrl', 'title', 'date', 'id'],
            data: [
                {id:'20',imgUrl: 'http://za.wx.joymap.cn/za-app/resources/images/wzb/1_thumb.jpg',  title: "众安保险开业仪式微直播", date:"2013-11-06"}
            ]
        },
        
        itemTpl: '<div class="articleBox" style="margin:8px 0px 8px 0px;padding:5px;background:#ffffff;border:2px 0px 2px 0px;box-shadow: 3px 3px 3px 0 #999999;-webkit-box-shadow: 3px 3px 3px 0 #999999;-moz-box-shadow: 3px 3px 3px 0 #999999;">'+
        	'<span style="float:left;width:60px;height:50px;"><img src="{imgUrl}" style="width:100%;height:100%;" /></img></span>'+
        	'<div style="float:left;margin-left:5px;width:75%;"><div style="font-size:14px;width:100%;height:35px;font-size:13px;word-wrap: break-word; word-break: normal;">{title}</div><div style="height:15px;line-height:15px;font-size:11px;">{date}</div></div><em style="clear: both;display: block;"></em></div>',
        	
       listeners: {
                itemtap: function(data,index){
                       var record = data.getStore().getAt(index);
                       Ext.PageCacheObject.Ctl.loadArticleDetailPage(record.data.id);
             }
       } 	
    }
});
