Ext.define('za.view.ArticlePage', {
    extend: 'Ext.Panel',
    xtype: "articlepage",
    requires: [
               'za.view.NavigationBar'
           ],
    config: {
        fullscreen: true,   
        scrollable: true,
        items: [
            {
                id: 'articlepagescreen',
                xtype: 'panel',
                style: "width:100%;-webkit-text-size-adjust: none;-webkit-user-select: none;padding:0px;margin: 0; background: #ffffff;background-size: cover;",
                html:  '<div id="tmpHeightContent"></div>'+
                		'<div id="articlepagecontentdiv" style="position:absolute;font-size:17px;font-weight:2em;padding:5px;margin-bottom:20px;line-height:22px;color:#706e6f;top:150px;">'+
                		'<div id="articlePageTitle" style="margin-top:10px;font-size:15px;font-weight:bold;"></div>' +
			    		'<div id="articlePageDate" style="margin:10px 0px 10px 0px;font-size:12px;"></div>'+
			    		'<div id="articlePageContent" style="font-size:14px;margin-bottom:20px;"></div>' +
                		'</div>'
            },
            {
                id: 'articlePageToolbar',
                xtype: 'pagenavigationbar',
                title: '',
                docked: 'top',
                items: [/*{
                    xtype : 'button',
                    id: 'viewSourceButton',
                    hidden: false,
                    align : 'right',
                    action: 'backToHomePage',
                    text  : '首页'
                },*/
                {
                    text: 'Back',
                    ui: 'back',
                    align : 'left',
                    action: 'backToHistory'
                }
                ]
            }
        ]
    }
});
