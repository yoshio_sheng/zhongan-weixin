/**
 * Demonstrates a tabbed form panel. This uses a tab panel with 3 tabs - Basic, Sliders and Toolbars - each of which is
 * defined below.
 *
 * See this in action at http://dev.sencha.com/deploy/sencha-touch-2-b3/examples/kitchensink/index.html#demo/forms
 */
Ext.define('za.view.BonusFormView', {
    extend: 'Ext.form.Panel',
    xtype: "bonusformview",
    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Spinner',
        'Ext.field.Password',
        'Ext.field.Email',
        'Ext.field.Url',
        'Ext.field.DatePicker',
        'Ext.field.Select',
        'Ext.field.Hidden',
        'Ext.field.Radio'
    ],
    config: {
        items: [
            {
                xtype: 'fieldset',
                id: 'fieldset3',
                defaults: {
                    labelWidth: '35%'
                },
                items: [
                    {
					    xtype         : 'panel',
					    style:"padding: .5em;",
					    html:"恭喜您中奖了，请在下方输入您的淘宝ID，或身份证号码（任意填写一个即可）。我们将尽快与您取得联系"
					},
					{
					    xtype         : 'panel',
					    style:"padding: .5em;padding-bottom:0px;",
					    html:"淘宝ID：<span id='taobaoerror' class='errorfieldarea'></span>"
					},
                    {
                        xtype         : 'textfield',
                        name          : 'taobao',
                        label         : '',
                        placeHolder   : '',
                        required      : true,
                        clearIcon     : true
                    },
                    {
					    xtype         : 'panel',
					    style:"padding: .5em;padding-bottom:0px;",
					    html:"身份证号：<span id='idcarderror' class='errorfieldarea'></span>"
					},
                    {
                        xtype      : 'textfield',
                        name       : 'idcard',
                        label      : '',
                        placeHolder: '',
                        required      : true,
                        clearIcon  : true
                    }
                ]
            },
            {
                xtype: 'container',
                defaults: {
                    xtype: 'button',
                    style: 'width:70%;height:50px;line-height:50px;',
                    flex : 1
                },
                layout: {
                    type: 'hbox'
                },
                items: [
					{
						 xtype: "spacer"
					},
                    {
                    	id:"bonusSubmitButton",
                        text: '提 交'
                    },
                    {
                   	 xtype: "spacer"
                    }
                ]
            }
        ]
    }
});
