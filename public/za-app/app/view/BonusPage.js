Ext.define('za.view.BonusPage', {
    extend: 'Ext.Container',
    xtype: 'bonusPage',

    requires: [
        'za.view.NavigationBar'
    ],

    config: {
        fullscreen: true,
        
        layout: {
            type: 'card'
        },

        items: [
            {
            	id: 'bonusForm',
            	xtype: 'bonusformview',
                scrollable: true
            },
            {
                xtype: 'pagenavigationbar',
                title: '有奖问答',
                docked: 'top',
                items: [{
                    xtype : 'button',
                    hidden: false,
                    align : 'right',
                    action: 'backToHomePage',
                    text  : '首页'
                },
                {
                    text: 'Back',
                    ui: 'back',
                    align : 'left',
                    action: 'backToHistory'
                }
                ]
            }
        ]
    }
});
