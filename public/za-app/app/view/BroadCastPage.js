Ext.define('za.view.BroadCastPage', {
    extend: 'Ext.Container',
    xtype: 'broadCastPage',
    requires: [
       'za.view.NavigationBar',
       'za.view.ArticleListView'
   ],
    config: {
        fullscreen: true,
        layout: {
            type: 'card'
        },
        
        items: [
	        {
	        	id:"articleListContainer",
	            xtype: 'articlelist',
	            style: "background:#F0F8FF;background:none;margin:0px;padding:0px;padding-top:50px;background:url('/za-app/bg_question.jpg');background-size:cover;",
//	            flex: 2,
	            scrollable: true
	        }/*,
            {
                xtype: 'pagenavigationbar',
                title: '品牌动态',
                docked: 'top',
                items: [{
                    xtype : 'button',
                    hidden: false,
                    align : 'right',
                    action: 'backToHomePage',
                    text  : '首页'
                },
                {
                    text: 'Back',
                    ui: 'back',
                    align : 'left',
                    action: 'backToHistory'
                }
                ]
            }*/
        ]
    }
});
