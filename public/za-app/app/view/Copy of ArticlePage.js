Ext.define('za.view.ArticlePage', {
    extend: 'Ext.Panel',
    xtype: "articlepage",
    requires: [
               'za.view.NavigationBar'
           ],
    config: {
    	fullscreen: true,
    	scrollable: true,
        items: [
			{
			    id: 'articlepagescreen',
			    xtype: 'panel',
			    style: "width:90%;-webkit-text-size-adjust: none;-webkit-user-select: none;padding: 10px 0 0 0;margin: 0; background: #ffffff;margin:0 auto;",
			    html: '<div id="articlePageTitle" style="margin-top:10px;font-size:15px;font-weight:bold;"></div>' +
			    		'<div id="articlePageDate" style="margin:10px 0px 10px 0px;font-size:12px;"></div>'+
			    		'<div id="articlePageContent" style="font-size:14px;margin-bottom:20px;"></div>'
			},
            {
				id: 'articlePageToolbar',
                xtype: 'pagenavigationbar',
                title: '',
                docked: 'top',
                items: [/*{
                    xtype : 'button',
                    id: 'viewSourceButton',
                    hidden: false,
                    align : 'right',
                    action: 'backToHomePage',
                    text  : '首页'
                },*/
                {
                    text: 'Back',
                    ui: 'back',
                    align : 'left',
                    action: 'backToHistory'
                }
                ]
            }
        ]
    }
});
