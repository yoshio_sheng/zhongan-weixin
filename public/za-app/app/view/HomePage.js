Ext.define('za.view.HomePage', {
    extend: 'Ext.Panel',
    xtype: "homepage",

    config: {
    	fullscreen: true,
        
        layout: {
        	layout: 'fit'
        },
        
        items: [
			{
			    id: 'homepagescreen',
			    xtype: 'panel',
			    style: "width:100%;height:100%;-webkit-text-size-adjust: none;-webkit-user-select: none;padding: 10px 0 0 0;margin: 0; background: url(/za-app/home_bg.jpg);background-size: cover;",
			    html: Ext.os.is.Phone ? '<div class="box">'+
			    		'<a class="box-item" style="cursor:pointer;box-shadow: 3px 3px 3px 0 #999999;-webkit-box-shadow: 3px 3px 3px 0 #999999;-moz-box-shadow: 3px 3px 3px 0 #999999;" ontouchstart="Ext.PageCacheObject.Ctl.loadPageModule(\'register\');">'+
							'<div class="txt"></div>'+
							'<div class="bg" style="background:url(http://zhongan.b0.upaiyun.com/1.3/msite/btn_register.jpg);background-size: cover;"></div>'+
						'</a>'+
						'<a class="box-item" style="cursor:pointer;box-shadow: 3px 3px 3px 0 #999999;-webkit-box-shadow: 3px 3px 3px 0 #999999;-moz-box-shadow: 3px 3px 3px 0 #999999;" ontouchstart="Ext.PageCacheObject.Ctl.loadPageModule(\'ask\');">'+
							'<div class="txt"></div>'+
							'<div class="bg" style="background:url(http://zhongan.b0.upaiyun.com/1.3/msite/btn_ask.jpg);background-size: cover;"></div>'+
						'</a>'+
						'<a class="box-item" style="cursor:pointer;box-shadow: 3px 3px 3px 0 #999999;-webkit-box-shadow: 3px 3px 3px 0 #999999;-moz-box-shadow: 3px 3px 3px 0 #999999;" ontouchstart="Ext.PageCacheObject.Ctl.loadPageModule(\'zhibo\');">'+
							'<div class="txt"></div>'+
							'<div class="bg" style="background:url(http://zhongan.b0.upaiyun.com/1.3/msite/btn_brand_event.jpg);background-size: cover;"></div>'+
						'</a>'+
						'<a class="box-item" style="box-shadow: 3px 3px 3px 0 #999999;-webkit-box-shadow: 3px 3px 3px 0 #999999;-moz-box-shadow: 3px 3px 3px 0 #999999;" ontouchstart="Ext.PageCacheObject.Ctl.loadPageModule(\'about\');">'+
							'<div class="txt"></div>'+
							'<div class="bg" style="background:url(http://zhongan.b0.upaiyun.com/1.3/msite/btn_about_us.jpg);background-size: cover;"></div>'+
						'</a>'+
						'<em style="clear: both;display: block;"></em>'+
					'</div>' : 
					'<div class="box">'+
						'<a class="box-item" style="cursor:pointer;box-shadow: 3px 3px 3px 0 #999999;-webkit-box-shadow: 3px 3px 3px 0 #999999;-moz-box-shadow: 3px 3px 3px 0 #999999;" onclick="Ext.PageCacheObject.Ctl.loadPageModule(\'register\');">'+
							'<div class="txt"></div>'+
							'<div class="bg" style="background:url(http://zhongan.b0.upaiyun.com/1.3/msite/btn_register.jpg);background-size: cover;"></div>'+
						'</a>'+
						'<a class="box-item" style="cursor:pointer;box-shadow: 3px 3px 3px 0 #999999;-webkit-box-shadow: 3px 3px 3px 0 #999999;-moz-box-shadow: 3px 3px 3px 0 #999999;" onclick="Ext.PageCacheObject.Ctl.loadPageModule(\'ask\');">'+
							'<div class="txt"></div>'+
							'<div class="bg" style="background:url(http://zhongan.b0.upaiyun.com/1.3/msite/btn_ask.jpg);background-size: cover;"></div>'+
						'</a>'+
						'<a class="box-item" style="cursor:pointer;box-shadow: 3px 3px 3px 0 #999999;-webkit-box-shadow: 3px 3px 3px 0 #999999;-moz-box-shadow: 3px 3px 3px 0 #999999;" onclick="Ext.PageCacheObject.Ctl.loadPageModule(\'zhibo\');">'+
							'<div class="txt"></div>'+
							'<div class="bg" style="background:url(http://zhongan.b0.upaiyun.com/1.3/msite/btn_brand_event.jpg);background-size: cover;"></div>'+
						'</a>'+
						'<a class="box-item" style="box-shadow: 3px 3px 3px 0 #999999;-webkit-box-shadow: 3px 3px 3px 0 #999999;-moz-box-shadow: 3px 3px 3px 0 #999999;" onclick="Ext.PageCacheObject.Ctl.loadPageModule(\'about\');">'+
							'<div class="txt"></div>'+
							'<div class="bg" style="background:url(http://zhongan.b0.upaiyun.com/1.3/msite/btn_about_us.jpg);background-size: cover;"></div>'+
						'</a>'+
						'<em style="clear: both;display: block;"></em>'+
					'</div>'	
			}
        ]
    }
});
