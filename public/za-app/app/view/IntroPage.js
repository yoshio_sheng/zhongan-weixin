Ext.define('za.view.IntroPage', {
    extend: 'Ext.Panel',
    xtype: "intropage",
    requires: [
               'za.view.NavigationBar'
           ],
    config: {
        fullscreen: true,   
        scrollable: true,
        items: [
            {
                id: 'intropagescreen',
                xtype: 'panel',
                style: "width:100%;-webkit-text-size-adjust: none;-webkit-user-select: none;padding:0px;margin: 0; background: #ffffff;background-size: cover;",
                html:  '<img id="aboutBgImg" src="http://zhongan.b0.upaiyun.com/1.6/msite/bg_about_us.jpg" style="width:100%;" />'+
                		'<div id="tmpHeightContent"></div>'+
                		'<div id="intropageContent" style="position:absolute;font-size:17px;font-weight:2em;padding:5px;margin-bottom:20px;line-height:22px;color:#706e6f;top:150px;">'+
                			'<div id="moreContent_4" style="text-align: justify;">　　11月6日，马云、马化腾、马明哲将首次齐聚复旦校园，参加“互联网金融”论坛暨众安保险开幕仪式，与嘉宾、学子及场外网友互动，共商互联网金融大计。'+
                			'<div id="moreContent_3" style="text-align: justify;">　　众安保险作为国内首家互联网保险公司，定位于“服务互联网”。众安保险不只是通过互联网销售既有的保险产品，而是通过产品创新，为互联网的经营者和参与者提供一系列整体解决方案。</div>'+
                			'<div id="moreContent_2" style="text-align: justify;">　　10月28日起，您可以通过众安官方微信平台中的<a style="color:#1D8599;" '+(Ext.os.is.Phone ? 'ontouchstart' : 'onclick') +'="Ext.PageCacheObject.Ctl.loadAskPage();" >“我要提问”</a>模块，向我们提出您宝贵的见解和问题，参与现场互动问答。马云、马化腾、马明哲将带您直击互联网金融的最前沿。</div>'+
							'<div id="moreContent_1" style="text-align: justify;">　　本次开业活动将会通过众安保险官方新浪微博，腾讯微博，微信平台以及来往平台进行现场直播，届时敬请积极参与。</div>'+
                            '<div id="moreContent_0" style="text-align: justify;"></br>    <a style="color:#1D8599;" '+(Ext.os.is.Phone ? 'ontouchstart' : 'onclick') +'="Ext.PageCacheObject.Ctl.loadBroadcastPage();" >点击进入微直播</a></div>'+
                		'</div>'
            }/*,
            {
                id: 'articlePageToolbar',
                xtype: 'pagenavigationbar',
                title: '关于众安',
                docked: 'top',
                items: [{
                    xtype : 'button',
                    id: 'viewSourceButton',
                    hidden: false,
                    align : 'right',
                    action: 'backToHomePage',
                    text  : '首页'
                },
                {
                    text: 'Back',
                    ui: 'back',
                    align : 'left',
                    action: 'backToHistory'
                }
                ]
            }*/
        ]
    }
});
