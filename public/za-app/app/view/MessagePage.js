Ext.define('za.view.MessagePage', {
    extend: 'Ext.Panel',
    xtype: "messagepage",
    requires: [
               'za.view.NavigationBar'
           ],
    config: {
        fullscreen: true,   
        scrollable: false,
        items: [
            {
                id: 'messagepagescreen',
                xtype: 'panel',
                style: "width:100%;-webkit-text-size-adjust: none;-webkit-user-select: none;padding:0px;margin: 0; background: #ffffff;background-size: cover;",
                html:  '<img id="messageBgImg" src="http://zhongan.b0.upaiyun.com/1.5/msite/bg_question.jpg" style="width:100%;" />'+
                		'<div id="tmpHeightContent"></div>'+
                		'<div id="messagepageContent" style="position:absolute;font-size:17px;font-weight:2em;padding:5px;margin-bottom:20px;line-height:22px;color:#706e6f;top:150px;">'+
                			'<div id="messagebodyarea" style="text-align: justify;"></div><br/><br/>'+
                			'<div id="messagebtnarea"><div '+(Ext.os.is.Phone ? 'ontouchstart' : 'onclick') +'="Ext.PageCacheObject.Ctl.loadAskPage();" id="backAndContinueAskButton" style="-webkit-border-radius: 6px;-moz-border-radius: 6px;-ms-border-radius: 6px;-o-border-radius: 6px;border-radius: 6px;text-align:center;">继续提问</div></div>'+
                		'</div>'
            }/*,
            {
                id: 'articlePageToolbar',
                xtype: 'pagenavigationbar',
                title: '关于众安',
                docked: 'top',
                items: [{
                    xtype : 'button',
                    id: 'viewSourceButton',
                    hidden: false,
                    align : 'right',
                    action: 'backToHomePage',
                    text  : '首页'
                },
                {
                    text: 'Back',
                    ui: 'back',
                    align : 'left',
                    action: 'backToHistory'
                }
                ]
            }*/
        ]
    }
});
