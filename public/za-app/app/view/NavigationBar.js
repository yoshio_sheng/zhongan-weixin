Ext.define('za.view.NavigationBar', {
    extend: 'Ext.TitleBar',
    xtype: 'pagenavigationbar',

    config: {
        ui: 'dark'
    },

    platformConfig: [{
        platform: 'blackberry',
        ui: 'light'
    }]
});
