/**
 * Demonstrates a tabbed form panel. This uses a tab panel with 3 tabs - Basic, Sliders and Toolbars - each of which is
 * defined below.
 *
 * See this in action at http://dev.sencha.com/deploy/sencha-touch-2-b3/examples/kitchensink/index.html#demo/forms
 */
Ext.define('za.view.QuestionFormView', {
    extend: 'Ext.form.Panel',
    xtype: "questionformview",
    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Spinner',
        'Ext.field.Password',
        'Ext.field.Email',
        'Ext.field.Url',
        'Ext.field.DatePicker',
        'Ext.field.Select',
        'Ext.field.Hidden',
        'Ext.field.Radio'
    ],
    config: {
        items: [
            {
                xtype: 'fieldset',
                id: 'fieldset2',
                style:"background:none;margin:0px;padding:0px;padding-top:50px;background:url('/za-app/bg_question.jpg');background-size:cover;-webkit-border-radius: 0px;-moz-border-radius: 0px;-ms-border-radius: 0px;-o-border-radius: 0px;border-radius: 0px;",
                defaults: {
                    labelWidth: '35%'
                },
                items: [
					{
					    xtype         : 'panel',
					    style:"padding: .5em;padding-bottom:0px;",
					    html:"提出的问题：<span id='questionerror' class='errorfieldarea'></span>"
					},
                    {
                        xtype         : 'textareafield',
                        name          : 'question',
                        label         : '',
                        placeHolder   : '',
                        required      : true,
                        clearIcon     : false
                    },
                    {
					    xtype         : 'panel',
					    style: "padding: .5em;padding-bottom:0px;",
					    html: "姓名：<span id='nameerror' class='errorfieldarea'></span>"
					},
                    {
                        xtype         : 'textfield',
                        name          : 'name',
                        label         : '',
                        placeHolder   : '',
                        required      : true,
                        clearIcon     : true
                    },
                    {
					    xtype         : 'panel',
					    style: "padding: .5em;padding-bottom:0px;",
					    html: "手机号：<span id='mobileerror' class='errorfieldarea'></span>"
					},
                    {
                        xtype      : 'textfield',
                        name       : 'mobile',
                        label      : '',
                        placeHolder: '',
                        required      : true,
                        clearIcon  : true
                    },
                    {
					    xtype         : 'panel',
					    style: "padding: .5em;background:#fafafa;opacity:1;",
					    html: "<span id='instTip' style='font-size:13px;'></span>"
					}
                ]
            },
            /*
            {
                xtype: 'container',
                defaults: {
                    xtype: 'button',
                    style: 'width:100%;',
                    flex : 2
                },
                layout: {
                    type: 'hbox'
                },
                items: [
					{
					    xtype: 'spacer'
					},
                    {
                    	id:"questionSubmitButton",
                        text: '提交'
                    },
                    {
					    xtype: 'spacer'
					},
                    {
                    	id:"questionContinueButton",
                        text: '继续提问'
                    },
					{
					    xtype: 'spacer'
					}
                ]
            }*/
            {
                xtype: 'container',
                defaults: {
                    xtype: 'button',
                    style: 'width:70%;height:50px;line-height:50px;background:#fafafa;opacity:1;',
                    flex : 1
                },
                layout: {
                    type: 'hbox'
                },
                items: [
					{
						 xtype: "spacer"
					},
                    {
                    	id:"questionSubmitButton",
                        text: '提 交'
                    },
                    {
                   	 xtype: "spacer"
                    }
                ]
            },
            {
            	xtype:"panel",
            	style: "height:150px;background:#fafafa;opacity:1;",
            	html:"<div style='min-height:150px;'></div>"
            }
        ]
    }
});
