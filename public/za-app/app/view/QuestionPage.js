Ext.define('za.view.QuestionPage', {
    extend: 'Ext.Container',
    xtype: 'questionPage',

    requires: [
        'za.view.NavigationBar'
    ],

    config: {
        fullscreen: true,
        layout: {
            type: 'card'
        },
//        style: "background:url('/za-app/bg_question.jpg');background-size:cover;",
        items: [
            {
            	id: 'questionForm',
            	xtype: 'questionformview',
                scrollable: true
//                style:"background:none;padding-top:50px;background:url('/za-app/bg_question.jpg');background-size:cover;"
            }
            /*,
            {
                xtype: 'pagenavigationbar',
                title: '向三马提问',
                docked: 'top',
                items: [{
                    xtype : 'button',
                    hidden: false,
                    align : 'right',
                    action: 'backToHomePage',
                    text  : '首页'
                },
                {
                    text: 'Back',
                    ui: 'back',
                    align : 'left',
                    action: 'backToHistory'
                }
                ]
            }*/
        ]
    }
});
