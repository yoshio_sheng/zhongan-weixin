/**
 * Demonstrates a tabbed form panel. This uses a tab panel with 3 tabs - Basic, Sliders and Toolbars - each of which is
 * defined below.
 *
 * See this in action at http://dev.sencha.com/deploy/sencha-touch-2-b3/examples/kitchensink/index.html#demo/forms
 */
Ext.define('za.view.RegisterFormView', {
    extend: 'Ext.form.Panel',
    xtype: "registerformview",
    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Spinner',
        'Ext.field.Password',
        'Ext.field.Email',
        'Ext.field.Url',
        'Ext.field.DatePicker',
        'Ext.field.Select',
        'Ext.field.Hidden',
        'Ext.field.Radio'
    ],
    config: {
        items: [
            {
                xtype: 'fieldset',
                id: 'fieldset1',
                defaults: {
                    labelWidth: '35%'
                },
                items: [
					{
					    xtype         : 'panel',
					    style:"padding: .5em;padding-bottom:0px;",
					    html:"姓名：<span id='usernameerror' class='errorfieldarea'></span>"
					},
                    {
                        xtype         : 'textfield',
                        name          : 'name',
                        label         : '',
                        placeHolder   : '',
                        required      : true,
                        clearIcon     : true
                    },
                    {
					    xtype         : 'panel',
					    style:"padding: .5em;padding-bottom:0px;font-size:12px;color:#2518B5;",
					    html:"为保障个人权益请输入真实姓名，姓名请与证件保持一致，入场时请带上您的证件"
					},
                    {
					    xtype         : 'panel',
					    style:"padding: .5em;padding-bottom:0px;",
					    html:"学校名称：<span id='schoolnameerror' class='errorfieldarea'></span>"
					},
                    {
                        xtype      : 'textfield',
                        name       : 'schoolname',
                        label      : '',
	                    placeHolder   : '',
	                    required      : true,
	                    clearIcon     : true
                    },
                    {
					    xtype         : 'panel',
					    style:"padding: .5em;padding-bottom:0px;",
					    html:"专业：<span id='schoollevelerror' class='errorfieldarea'></span>"
					},
                    {
                        xtype      : 'textfield',
                        name       : 'schoollevel',
                        label      : '',
	                    placeHolder   : '',
	                    required      : true,
	                    clearIcon     : true
                    },
                    {
					    xtype         : 'panel',
					    style:"padding: .5em;padding-bottom:0px;",
					    html:"学生证号：<span id='schoolcarderror' class='errorfieldarea'></span>"
					},
                    {
                        xtype      : 'textfield',
                        name       : 'schoolcard',
                        label      : '',
                        placeHolder: '',
                        required      : true,
                        clearIcon  : true
                    },
                    {
					    xtype         : 'panel',
					    style:"padding: .5em;padding-bottom:0px;",
					    html:"手机号：<span id='usermobileerror' class='errorfieldarea'></span>"
					},
                    {
                        xtype      : 'textfield',
                        name       : 'mobile',
                        label      : '',
                        placeHolder: '',
                        required      : true,
                        clearIcon  : true
                    }
					/*,
                    {
					    xtype         : 'panel',
					    style:"padding: .5em;padding-bottom:0px;",
					    html:"身份证号：<span id='idcarderror' class='errorfieldarea'></span>"
					},
                    {
                        xtype      : 'textfield',
                        name       : 'idcard',
                        label      : '',
                        placeHolder: '',
                        required      : true,
                        clearIcon  : true
                    }*/
                ]
            },
            {
                xtype: 'container',
                defaults: {
                    xtype: 'button',
                    style: 'width:70%;height:50px;line-height:50px;',
                    flex : 1
                },
                layout: {
                    type: 'hbox'
                },
                items: [
                     {
                    	 xtype: "spacer"
                     },
                    {
                    	id:"registerSubmitButton",
                        text: '报 名'
                    },
                    {
                   	 xtype: "spacer"
                    }
                ]
            }
        ]
    }
});
