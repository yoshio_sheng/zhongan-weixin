Ext.define('za.view.RegisterPage', {
    extend: 'Ext.Container',
    xtype: 'registerPage',

    requires: [
        'za.view.NavigationBar'
    ],

    config: {
        fullscreen: true,
        
        layout: {
            type: 'card'
        },

        items: [
            {
            	id: 'registerForm',
            	xtype: 'registerformview',
                scrollable: true
            },
            {
                xtype: 'pagenavigationbar',
                title: '在线报名',
                docked: 'top',
                items: [{
                    xtype : 'button',
                    id: 'viewSourceButton',
                    hidden: false,
                    align : 'right',
                    action: 'backToHomePage',
                    text  : '首页'
                },
                {
                    text: 'Back',
                    ui: 'back',
                    align : 'left',
                    action: 'backToHistory'
                }
                ]
            }
        ]
    }
});
